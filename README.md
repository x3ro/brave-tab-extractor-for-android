# Android Brave Tab Extractor

## Usage

```bash
cd ~
su
cp /data/data/com.brave.browser/app_tabs/0/tab_state0 ./
exit
./tabs > open-tabs.txt
```

## Cross-Compile for Android

To cross-compile for android (target `arm-linux-androideabi`) install `cross`:

```bash
cargo install cross
```

then compile with `cross`:

```bash
cross build --target=arm-linux-androideabi --release --verbose
```
