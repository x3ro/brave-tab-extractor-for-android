use byteorder::{BigEndian, ByteOrder};
//use byteorder::ByteOrder;
use std::io::{BufReader, Read, Seek, SeekFrom};
use std::mem::size_of;
use std::{error::Error, fs::File};

fn main() {
    let fname = "tab_state0";
    let file = File::open(fname).unwrap();
    let mut reader = BufReader::new(file);

    // Skip until list of tab begins
    reader.seek(SeekFrom::Start(20)).unwrap();

    while let Ok(Some(url)) = read_next(&mut reader) {
        println!("{}", url);
    }
}

fn read_next<T: Read + Seek>(file: &mut T) -> Result<Option<String>, Box<dyn Error>> {
    // Skip unknown bytes
    file.seek(SeekFrom::Current(4))?;
    // Read string length
    let buf = read_bytes(file, size_of::<u16>())?;
    let len = BigEndian::read_i16(&buf);
    // Read url
    let buf = read_bytes(file, len as usize)?;
    let url = String::from_utf8(buf)?;

    Ok(Some(url))
}

fn read_bytes(file: &mut dyn Read, len: usize) -> Result<Vec<u8>, std::io::Error> {
    let mut buf = Vec::with_capacity(len);
    let n = file.take(len as u64).read_to_end(&mut buf)?;

    if n < len {
        return Err(std::io::Error::new(
            std::io::ErrorKind::UnexpectedEof,
            format!("Trying to read {} bytes while there were only {} bytes left before EOF", len, n),
        ));
    }

    Ok(buf)
}
